/**
 * @author zenglongfei
 * @time 2018/8/8
 * @version v3.0.0
 * @class
 */
const testUrl = '/api/test2';

const data = {
  name: '李大头',
  address: '广州天河区'
};

module.exports = {
  [`POST ${testUrl}`](req, res) {
    res.json(data);
  },
  [`GET ${testUrl}`](req, res) {
    res.json(data);
  }
};