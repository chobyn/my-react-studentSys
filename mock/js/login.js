/**
 * @author zenglongfei
 * @time 2018/7/16
 * @version v3.0.0
 * @class
 */

const loginUrl = '/api/login';

const user = {
  name: 'longfei',
  id: 'jsdkajdlasjldsa'
};

module.exports = {
  [`POST ${loginUrl}`](req, res) {
    res.json(user);
  },
  [`GET ${loginUrl}`](req, res) {
    res.json(user);
  }
};