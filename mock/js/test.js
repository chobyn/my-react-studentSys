/**
 * @author zenglongfei
 * @time 2018/8/8
 * @version v3.0.0
 * @class
 */
const testUrl = '/api/test';

const data = {
  name: '广州华资软件技术有限公司',
  address: '广州天河区'
};

module.exports = {
  [`POST ${testUrl}`](req, res) {
    res.json(data);
  },
  [`GET ${testUrl}`](req, res) {
    res.json(data);
  }
};