const jsonServer = require('json-server');
const fs = require('fs');
const path = require('path');

const jsonsFiles = [];
fs.readdirSync(path.join(__dirname + '/json')).forEach((file) => {
  jsonsFiles.push(`json/${file}`);
});

const jsonKeys = [];
const jsonServers = [];
for (const index in jsonsFiles) {
  if (Object.prototype.hasOwnProperty.call(jsonsFiles, index)) {
    jsonServers.push(jsonServer.router(jsonsFiles[index]));

    const aJson = require(`./${jsonsFiles[index]}`);
    for (const key in aJson) {
      if (Object.prototype.hasOwnProperty.call(aJson, key)) {
        jsonKeys.push({ key, index });
      }
    }
  }
}

function applyJsonServer(server) {
  server.use((req, res, next) => {
    for (const index in jsonKeys) {
      if (Object.prototype.hasOwnProperty.call(jsonKeys, index)) {
        const aObject = jsonKeys[index];
        if (req.originalUrl.indexOf(`/${aObject.key}/`) === 0 || req.originalUrl === `/${aObject.key}`) {
          req.url = `/defaultJsonServers/${aObject.index}${req.originalUrl}`;
          break;
        }
      }
    }
    next();
  });

  for (const index in jsonServers) {
    if (Object.prototype.hasOwnProperty.call(jsonServers, index)) {
      server.use(`/defaultJsonServers/${index}`, jsonServers[index]);
    }
  }
}

module.exports = applyJsonServer;
