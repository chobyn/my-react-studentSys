# sinogear-frontend-starter

应用开发框架SinoGear赛姬的脚手架项目

## 目录介绍

```
├── .gitattributes   // 控制git在check in/out时对文件的操作
├── .gitignore       // git的忽略列表
├── README.md         // 本介绍文档
├── .eslintrc              // eslint的规则文件
├── .eslintignore          // eslint语法检查忽略文件、
├── .editorconfig          // 
├── .prettierrc.js          // 自动格式化代码文件
├── .webpackrc.js          // webpack基础配置
├── jest.json          // 测试配置
├── package.json          // 安装模块
├── package-lock.json          // 安装模块锁定文件
├── roadhogrc.build.js       // 编译配置文件
├── theme.config.js
├── node_modules      // node的模块存放目录，安装了node模块后自动出现
│   ├── cz-conventional-changelog
│   ├── ...
│── src               // 源码目录
│   ├── common        // 公共文件目录
│   │   ├── config.js        // 全局配置文件
│   │   ├── Constants.js     // 全局类型常量文件
│   │   ├── menu.js          // 全局菜单文件   
│   │   └── router.js        // 路由配置文件
│	├── components           // 业务公共组件目录
│	├── modules               // 业务模块目录
│   │   ├── home             // 首页模块目录
│   │   │	 ├── models     
│	│	│	 ├── services  
│	│	│	 └── views 
│	│	│		  ├── HomePage.js
│	│	│		  └── HomePage.less
│ 	│   │               
│   │   └── layouts           // 布局模块目录
│   │   	 	 ├── models
│   │        │    └── global.js 
│	│		 ├── services  
│	│		 │    └── global.js
│	│		 └── views 
│	│			  ├── BasicLayout.js
│	│			  └── Layout.js    
│	├── resources                   // 资源文件目录
│   │   ├── fonts
│   │   │   ├── font_antd3.0.3.eot
│	│	│	├── font_antd3.0.3.svg
│   │   │   ├── font_antd3.0.3.ttf
│	│	│	└── font_antd3.0.3.woff
│   │   │       
│   │   ├── images        
│   │   │    └── sinogear-logo.png     
│   │   └── svg
│   ├── themes                      // 主题样式配置目录
│	│	├── blue.less
│   │   ├── default.less   
│   │   ├── index.less       
│   │   ├── red.less
│   │   ├── vars.less                
│   │   	└── mixin.less        
│	├── utils                        // 主题样式配置目录
│	│	├── auth.js
│   │   ├── baseService.js   
│   │   ├── func.js       
│   │   └── Message.js
│   ├── index.ejs
│   ├── index.js                     // 项目入口页面
│   ├── index.less                   // 项目整体样式
│   ├── router.js                    // 项目路由入口
│   └── theme.js 


```


### 安装
有待说明

> 将提供yarn命令安装脚手架项目及模块

### 运行

使用`yarn`命令：

```
yarn run dev-mock
```
将以开发者模式启动该项目。启动成功后，可在浏览器内输入：`http://localhost:8000/`进行访问测试。


### 打包

使用`yarn`命令：

```
yarn run build
```
将项目打包至`dist`文件夹下。部署时直接使用该文件下文件即可。