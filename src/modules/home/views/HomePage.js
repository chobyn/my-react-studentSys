import React, { Component } from 'react';
import './HomePage.less';

class HomePage extends Component {
  render() {
    return (
      <div style={{ minHeight: 500, height: '60%' }}>
        <div style={{ textAlign: 'center', marginTop: '30px' }}>
          <img src={require('../../../resources/images/sinogear-logo.png')} />
          <h1> Hello SinoGear! </h1>
        </div>
      </div>
    );
  }
}

export default HomePage;
