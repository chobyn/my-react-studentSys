/**
 * @author liuqiubin
 * @time 2018/8/8
 * @version v3.0.0
 * @class
 */

import { getData } from '../services/test2'
export default {
  namespace: 'test2',

  state: {
    data: {
      name: '李**',
      address: '广州***'
    }
  },

  effects: {
    //action后对数据的获取
    *changeData2(_, { put, call }) {
      const data = yield call(getData);
      yield put ({
        type: 'changeDataEnd',
        payload: data
      })
    }
  },

  reducers: {
    //state的更新
    changeDataEnd(state, { payload }) {
      console.info(state);
      return {
        ...state,
        data: { ...payload }
      }
    }
  }
};
