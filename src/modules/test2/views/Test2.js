/**
 * @author liuqiubin
 * @time 2018/8/8
 * @version v3.0.0
 * @class
 */

import React, { PureComponent } from 'react';
import { connect } from 'dva';
import { Button } from 'antd';
import styles from './Test2.less';

@connect(( { test2 } ) => ({
  data: test2.data
}))
class Test2 extends PureComponent {
  onChange = () => {
    this.props.dispatch({ type: 'test2/changeData2' })
  }

  render() {
    const { data } = this.props;
    const { name = '', address = '' } = data;
    return (
      <div className={styles['huazi-test2_span']}>
        <div>name: {name}</div>
        <div>address: {address}</div>
        <div>
          <Button type="info" onClick={this.onChange}> 刷新</Button>
        </div>
      </div>
    )
  }
}
export default Test2;
