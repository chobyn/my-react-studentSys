/** reducer
 * @author liuqiubin
 * @time 2018/8/18
 * @version
 * @class
 */
import {INIT_DATA,QUERY_DATA, CLEAR_DATA,ADD_DATA,GET_PAGE_DATA,CHANGE_DATA,DELETE_DATA} from "../constants/Constants";

// models
export default function(state, action) {
  if (!state) {
    state = {
      studentData: [],
      pageSize: 10,
      totalCount: 0,
      queryStr: ''
    };
  }
  switch (action.type) {
    case INIT_DATA:
      // 初始化数据
      return {
        studentData: action.studentData,
        totalCount: action.studentData.length,
        pageSize: state.pageSize,
        queryStr: state.queryStr
      };
    case CLEAR_DATA:
      // 清除数据
      return {
        studentData: action.studentData,
        totalCount: 0,
        pageSize: state.pageSize,
        queryStr: ''
      };
    case QUERY_DATA:
      // 查询的数据
      return {
        studentData: action.studentData,
        totalCount: action.studentData.length,
        pageSize: state.pageSize,
        queryStr: action.queryStr
      };
    case ADD_DATA:
      // 新增的数据
      return {
        studentData: [...state.studentData, action.studentData],
        totalCount: state.totalCount + 1,
        pageSize: state.pageSize,
        queryStr: state.queryStr
      };
    case GET_PAGE_DATA:
      // 获取一页数据
      return {
        studentData: action.studentData,
        totalCount: state.totalCount,
        pageSize: state.pageSize,
        queryStr: state.queryStr
      };
    case CHANGE_DATA:
      // 修改后的数据
      return {
        studentData: [
          ...state.studentData.slice(0, action.index),
          action.studentData,
          ...state.studentData.slice(action.index + 1)
        ],
        totalCount: state.totalCount,
        pageSize: state.pageSize,
        queryStr: state.queryStr
      };
    case DELETE_DATA:
      // 删除后的数据
      return {
        studentData: [
          ...state.studentData.slice(0,action.index),
          ...state.studentData.slice(action.index+1)
        ],
        totalCount: state.totalCount - 1,
        pageSize: state.pageSize,
        queryStr: state.queryStr
      };
    default:
      return state;
  }
}

// action creators
export const initData = (studentData) => {
  return { type: INIT_DATA, studentData };
};
export const queryData = (studentData, queryStr) => {
  return { type: QUERY_DATA, studentData, queryStr };
};
export const clearData = (studentData) => {
  return { type: CLEAR_DATA, studentData };
};
export const addData = (studentData) => {
  return { type: ADD_DATA, studentData };
};
export const getPageData = (studentData) => {
  return { type: GET_PAGE_DATA, studentData };
};
export const changeData = (studentData, index) => {
  return { type: CHANGE_DATA, studentData, index };
};
export const deleteData = (index) => {
  return { type: DELETE_DATA, index };
};
