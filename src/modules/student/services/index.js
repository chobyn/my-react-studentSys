import { io } from 'SinoGear';
import { config } from '../../../common/config';

/**
 * 返回字符串
 * @param val
 * @returns {string} 拼接后字符串
 */
export const addString = (val) => {
  return `${val} long`;
};

/**z
 * 拼接字符串
 * @returns {string} 拼接后字符串
 * @param str
 */
export const contactPath = (arr) => {
  let str = [];
  for (let i = 0; i < arr.length; i++) {
    str.push(arr[i]);
  }
  str = str.join('');
  return str;
};

/**
 * 初始化请求数据 {get}
 * @returns {studentData}
 */
export async function initIo(pageSize) {
  console.log(pageSize);
  const path = contactPath([`${config.jsonServerPath}/studentData`]);
  return io.get(path);
}

/**
 * 请求每页数据{get}
 * @returns {studentData}
 */
export async function getPageDataIo(pageSize, currentPage, queryStr) {
  const path = contactPath([
    `${config.jsonServerPath}/studentData?`,
    queryStr,
    '_page=',
    currentPage,
    '&_limit=',
    pageSize
  ]);
  return io.get(path);
}

/**
 * 查询数据{get}
 * @returns {studentData,queryStr：搜索条件 }
 */
export async function queryDataIo(form) {
  let str = [];
  for (const k in form) {
    if (form[k] !== '' && form[k] !== undefined) {
      str = contactPath([str, k, '=', form[k], '&']);
    }
  }
  if (str.length > 0) {
    const path = contactPath([`${config.jsonServerPath}/studentData?`, str]);
    return {
      data: io.get(path),
      queryStr: str
    };
  } else {
    return null;
  }
}

/**
 * 新增数据{post}
 * @returns {studentData}
 */
export async function addDataIo(form, queryStr) {
  // 数据对象
  const body = {
    number: form.number,
    name: form.name,
    class: form.class,
    ages: form.ages,
    gender: form.gender,
    hobby: form.hobby
  };
  const path = contactPath([`${config.jsonServerPath}/studentData?`, queryStr]);
  return io.post(path, body, { 'Content-Type': 'application/json', reqEvalJSON: true });
}

/**
 * 修改数据{put}
 * @returns {studentData}
 */
export async function changeDataIo(newData) {
  const path = contactPath([`${config.jsonServerPath}/studentData/`, newData.id]);
  return io.put(path, newData);
}

/**
 * 删除数据{delete}
 * @returns {studentData}
 */
export async function deleteDataIo(studentData, targetData) {
  let index = 0;
  const arr = studentData;
  for (let i = 0; i < arr.length; i++) {
    if (targetData.id === arr[i].id) {
      index = i;
    }
  }
  const path = contactPath([`${config.jsonServerPath}/studentData/`, targetData.id]);
  io.delete(path, targetData);
  return index;
}
