/** 表单面板的对话框
 * @author liuqiubin
 * @time 2018/8/15
 * @version
 * @class
 */

import React from 'react';
import { Form, Input, Modal } from 'antd';

const FormItem = Form.Item;

const CollectionCreateForm = Form.create()(
  class extends React.Component {
    render() {
      const { visible, onCancel, onCreate, form } = this.props;
      const { getFieldDecorator } = form;
      return (
        <Modal
          visible={visible}
          title="学生信息表格"
          okText="确定"
          onCancel={onCancel}
          onOk={onCreate}
        >
          <Form layout="vertical">
            <FormItem label="学号">
              {getFieldDecorator('number', {
                rules: [{ required: true, message: '此项必须填写!' }]
              })(
                <Input />
              )}
            </FormItem>
            <FormItem label="姓名">
              {getFieldDecorator('name', {
                rules: [{ required: true, message: '此项必须填写!' }]
              })(
                <Input />
              )}
            </FormItem>
            <FormItem label="班级">
              {getFieldDecorator('class', {
                rules: [{ required: true, message: '此项必须填写!' }]
              })(
                <Input />
              )}
            </FormItem>
            <FormItem label="年龄">
              {getFieldDecorator('ages', {
                rules: [{ required: true, message: '此项必须填写!' }]
              })(
                <Input />
              )}
            </FormItem>
            <FormItem label="性别">
              {getFieldDecorator('gender', {
                rules: [{ required: true, message: '此项必须填写!' }]
              })(
                <Input />
              )}
            </FormItem>
            <FormItem label="爱好">
              {getFieldDecorator('hobby')(<Input type="textarea" />)}
            </FormItem>
          </Form>
        </Modal>
      );
    }
  }
);

export default CollectionCreateForm;
