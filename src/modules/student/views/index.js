/** app入口
 * @author liuqiubin
 * @time 2018/8/17
 * @version
 * @class
 */
import React, { Component } from 'react';
import { createStore } from 'redux';
import { Provider } from 'react-redux';
import StudentApp from './StudentApp';
import studentReducer from '../models/index';

const store = createStore(studentReducer);

class Student extends Component {
  render() {
    return (
      <Provider store={store}>
        <StudentApp />
      </Provider>
    );
  }
}

export default Student;
