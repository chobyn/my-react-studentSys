/** 新增面板
 * @author liuqiubin
 * @time 2018/8/15
 * @version v
 * @class
 */
import React, { Component } from 'react';
import { Button } from 'antd';
import CollectionCreateForm from './EditDialog';
import styles from './Student.less';
export class CollectionsPage extends Component {
  state = {
    visible: false
  };

  showModal = () => {
    this.setState({ visible: true });
  };

  handleCancel = () => {
    this.setState({ visible: false });
  };

  handleCreate = () => {
    const { form } = this.formRef.props;
    form.validateFields((err, values) => {
      if (err) {
        return;
      }
      if (this.props.handleFormData) {
        this.props.handleFormData(values);
      }
      form.resetFields();
      this.setState({ visible: false });
    });
  };

  saveFormRef = (formRef) => {
    this.formRef = formRef;
  };

  render() {
    return (
      <div className={styles['add-button']}>
        <Button type="primary" icon="plus" onClick={this.showModal}>
          新增
        </Button>
        <CollectionCreateForm
          wrappedComponentRef={this.saveFormRef}
          visible={this.state.visible}
          onCancel={this.handleCancel}
          onCreate={this.handleCreate}
        />
      </div>
    );
  }
}
