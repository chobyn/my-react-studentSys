/** 输入框表单，内含展开/隐藏 输入框
 * @author liuqiubin
 * @time 2018/8/15
 * @version
 * @class
 */

import React from 'react';
import { Form, Row, Col, Input, Button, Icon, Select } from 'antd';

const FormItem = Form.Item;

// 班级下拉框
const { Option } = Select;

export class AdvancedSearchForm extends React.Component {
  state = {
    expand: false
  };

  // To generate mock Form.Item
  getFields() {
    const count = this.state.expand ? 6 : 3;
    const { getFieldDecorator } = this.props.form;
    const children = [];
    children.push(
      <Col span={8} key={1} style={{ display: 'block' }}>
        <FormItem label="学号">
          {getFieldDecorator(`number`, {
            rules: [{}]
          })(<Input  />)}
        </FormItem>
      </Col>
    );
    children.push(
      <Col span={8} key={2} style={{ display: 'block' }}>
        <FormItem label="姓名">
          {getFieldDecorator(`name`, {
            rules: [{}]
          })(<Input />)}
        </FormItem>
      </Col>
    );
    children.push(
      <Col span={8} key={3} style={{ display: 'block' }}>
        <FormItem label="班级">
          {getFieldDecorator(
            'class', {
              initialValue: '',
            })
          (
            <Select>
              <Option value="">请选择班级</Option>
              <Option value="1151">1151</Option>
              <Option value="1152">1152</Option>
            </Select>
          )}
        </FormItem>
      </Col>
    );
    children.push(
      <Col span={8} key={4} style={{ display: [4] < count ? 'block' : 'none' }}>
        <FormItem label="年龄">
          {getFieldDecorator(`ages`, {
            rules: [{}]
          })(<Input  />)}
        </FormItem>
      </Col>
    );
    children.push(
      <Col span={8} key={5} style={{ display: [5] < count ? 'block' : 'none' }}>
        <FormItem label="性别">
          {getFieldDecorator(
            'sex', {
              initialValue: '',
            })
          (
            <Select>
              <Option value="">请选择性别</Option>
              <Option value="boy">boy</Option>
              <Option value="girl">girl</Option>
            </Select>
          )}
        </FormItem>
      </Col>
    );
    children.push(
      <Col span={8} key={6} style={{ display: [6] <= count ? 'block' : 'none' }}>
        <FormItem label="爱好">
          {getFieldDecorator(`hobby`, {
            rules: [{}]
          })(<Input />)}
        </FormItem>
      </Col>
    );
    return children;
  }
  handleSearch = (e) => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (this.props.toQueryData){
        this.props.toQueryData(values)
      }
    });
  };

  handleReset = () => {
    this.props.form.resetFields();
  };

  toggle = () => {
    const { expand } = this.state;
    this.setState({ expand: !expand });
  };
  render() {
    return (
      <Form className="ant-advanced-search-form" onSubmit={this.handleSearch}>
        <Row gutter={24}>{this.getFields()}</Row>
        <Row>
          <Col span={24} style={{ textAlign: 'right' }}>
            <Button type="primary" htmlType="submit">
              Search
            </Button>
            <Button style={{ marginLeft: 8 }} onClick={this.handleReset}>
              Clear
            </Button>
            <a style={{ marginLeft: 8, fontSize: 12 }} onClick={this.toggle}>
              Collapse <Icon type={this.state.expand ? 'up' : 'down'} />
            </a>
          </Col>
        </Row>
      </Form>
    );
  }
}
