/** student组件APP
 * @author liuqiubin
 * @time 2018/8/15
 * @version
 * @class
 */

// React库及React相关的第三方库
import React, { Component } from 'react';
import { connect } from 'react-redux';
// antd库
import { Form } from 'antd';
// 业务相关的库
import { CollectionsPage } from './AddContainer';
import { EditContainer } from './EditContainer';
import { AdvancedSearchForm } from './QueryContainer';
import { clearData, queryData, addData, initData, getPageData, changeData, deleteData } from '../models/index';
import { queryDataIo, addDataIo, initIo, getPageDataIo, changeDataIo, deleteDataIo } from '../services';
// 样式
import styles from './Student.less';

const WrappedAdvancedSearchForm = Form.create()(AdvancedSearchForm);

class StudentApp extends Component {
  static defaultProps = {
    studentData: []
  };
  constructor(props) {
    super(props);
    this.state = {
      pageSize: 10
    };
  }
  componentWillMount() {
    // 初始化数据
    const data = initIo(this.state.pageSize);
    data.then((result) => {
      this.props.initData(result);
    });
  }
  // 请求每页的数据
  getPageData(currentPage) {
    const data = getPageDataIo(this.state.pageSize, currentPage, this.props.queryStr);
    data.then((res) => {
      this.props.getPageData(res);
    });
  }
  // 查询数据
  toQueryData(form) {
    const theQueryData = queryDataIo(form);
    theQueryData.then((result) => {
      if (result) {
        result.data.then((value) => {
          if (value && value.length !== 0) {
            // 存在查找数据
            this.props.queryData(value, result.queryStr);
          } else {
            // 清空数据
            this.props.clearData([]);
            alert('未查到到相关结果！');
          }
        });
      }
    });
  }
  // 新增数据
  handleFormData(form) {
    // 数据对象
    const data = addDataIo(form, this.props.queryStr);
    data.then((result) => {
      this.props.addData(result);
    });
  }
  // 修改数据--在行中直接编辑修改
  changeData(newData) {
    const data = changeDataIo(newData);
    data.then((result) => {
      let index = 0;
      const arr = this.props.studentData;
      for (let i = 0; i < arr.length; i++) {
        if (result.id === arr[i].id) {
          index = i;
        }
      }
      this.props.changeData(result, index);
    });
  }
  // 修改数据--在点击按钮弹出面板后编辑修改
  editData(newData) {
    const data = changeDataIo(newData);
    data.then((result) => {
      let index = 0;
      const arr = this.props.studentData;
      for (let i = 0; i < arr.length; i++) {
        if (result.id === arr[i].id) {
          index = i;
        }
      }
      this.props.changeData(result, index);
    });
  }
  // 删除数据
  deleteData(targetData) {
    const data = deleteDataIo(this.props.studentData, targetData);
    data.then((index) => {
      this.props.deleteData(index);
    });
  }
  render() {
    return (
      <div className={styles['out-box']}>
        <div className={styles['in-box']}>
          <WrappedAdvancedSearchForm toQueryData={this.toQueryData.bind(this)} />
          <CollectionsPage handleFormData={this.handleFormData.bind(this)} />
          <EditContainer
            studentData={this.props.studentData}
            totalCount={this.props.totalCount}
            pageSize={this.props.pageSize}
            changeData={this.changeData.bind(this)}
            editData={this.editData.bind(this)}
            deleteData={this.deleteData.bind(this)}
            getPageData={this.getPageData.bind(this)}
          />
        </div>
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    studentData: state.studentData,
    loading: state.loading,
    visible: state.visible,
    totalCount: state.totalCount,
    pageSize: state.pageSize,
    queryStr: state.queryStr
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    initData: (studentData) => {
      dispatch(initData(studentData));
    },
    queryData: (studentData, queryStr) => {
      dispatch(queryData(studentData, queryStr));
    },
    clearData: (studentData) => {
      dispatch(clearData(studentData));
    },
    addData: (studentData) => {
      dispatch(addData(studentData));
    },
    getPageData: (studentData) => {
      dispatch(getPageData(studentData));
    },
    changeData: (studentData, index) => {
      dispatch(changeData(studentData, index));
    },
    deleteData: (index) => {
      dispatch(deleteData(index));
    }
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(StudentApp);
