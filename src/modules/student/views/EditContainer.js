/** 表格组件
 * @author liuqiubin
 * @time 2018/8/15
 * @version
 * @class
 */
import React from 'react';
import { Table, Input, Popconfirm, Form, Icon } from 'antd';
import CollectionCreateForm from './EditDialog';

const FormItem = Form.Item;
const EditableContext = React.createContext();

const EditableRow = ({ form, index, ...props }) => (
  <EditableContext.Provider value={form}>
    <tr {...props} />
  </EditableContext.Provider>
);

const EditableFormRow = Form.create()(EditableRow);

class EditableCell extends React.Component {
  state = {
    editing: false
  };

  componentDidMount() {
    if (this.props.editable) {
      document.addEventListener('click', this.handleClickOutside, true);
    }
  }

  componentWillUnmount() {
    if (this.props.editable) {
      document.removeEventListener('click', this.handleClickOutside, true);
    }
  }

  toggleEdit = () => {
    const editing = !this.state.editing;
    this.setState({ editing }, () => {
      if (editing) {
        this.input.focus();
      }
    });
  };

  handleClickOutside = (e) => {
    const { editing } = this.state;
    if (editing && this.cell !== e.target && !this.cell.contains(e.target)) {
      this.save();
    }
  };

  save = () => {
    const { record, handleSave } = this.props;
    this.form.validateFields((error, values) => {
      if (error) {
        return;
      }
      this.toggleEdit();
      handleSave({ ...record, ...values });
    });
  };

  render() {
    const { editing } = this.state;
    const { editable, dataIndex, title, record, index, handleSave, ...restProps } = this.props;
    return (
      <td ref={(node) => (this.cell = node)} {...restProps}>
        {editable ? (
          <EditableContext.Consumer>
            {(form) => {
              this.form = form;
              return editing ? (
                <FormItem style={{ margin: 0 }}>
                  {form.getFieldDecorator(dataIndex, {
                    rules: [
                      {
                        required: true,
                        message: `${title} is required.`
                      }
                    ],
                    initialValue: record[dataIndex]
                  })(<Input ref={(node) => (this.input = node)} onPressEnter={this.save} />)}
                </FormItem>
              ) : (
                <div className="editable-cell-value-wrap" style={{ paddingRight: 24 }} onClick={this.toggleEdit}>
                  {restProps.children}
                </div>
              );
            }}
          </EditableContext.Consumer>
        ) : (
          restProps.children
        )}
      </td>
    );
  }
}

export class EditContainer extends React.Component {
  constructor(props) {
    super(props);
    this.columns = [
      {
        title: '学号',
        dataIndex: 'number',
        width: '14%',
        editable: true
      },
      {
        title: '姓名',
        dataIndex: 'name',
        width: '14%',
        editable: true
      },
      {
        title: '班级',
        dataIndex: 'class',
        width: '14%',
        editable: true
      },
      {
        title: '年龄',
        dataIndex: 'ages',
        width: '14%',
        editable: true
      },
      {
        title: '性别',
        dataIndex: 'gender',
        width: '14%',
        editable: true
      },
      {
        title: '爱好',
        dataIndex: 'hobby',
        width: '14%',
        editable: true
      },
      {
        title: '操作',
        dataIndex: 'operation',
        render: (text, record) => {
          return (
            <span>
              <a onClick={() => this.handleEdit(record)} style={{ marginLeft: 15, marginRight: 15 }}>
                编辑
              </a>
              <Popconfirm title="确定要删除吗?" onConfirm={() => this.handleDelete(record)}>
                <a>删除</a>
              </Popconfirm>
            </span>
          );
        }
      }
    ];

    this.state = {
      dataSource: this.props.studentData,
      current: 1,
      visible: false,
      fieldsValue: {}
    };
  }
  // 编辑面板取消按钮
  handleCancel = () => {
    this.setState({ visible: false });
  };
  // 编辑面板点击提交
  handleCreate = () => {
    const { form } = this.formRef.props;
    form.validateFields((err, values) => {
      if (err) {
        return;
      }
      // 业务逻辑传给父组件
      if (this.props.editData) {
        values.id = this.state.fieldsValue.id;
        this.props.editData(values);
      }
      form.resetFields();
      this.setState({ visible: false });
    });
  };
  // 编辑面板 结束
  saveFormRef = (formRef) => {
    this.formRef = formRef;
  };

  // 计算当前有多少条数据
  handleTargetCount() {
    const total = this.props.totalCount;
    if (total <= 10) {
      return { currentCount: total };
    } else {
      if (this.state.current * 10 <= total) return { currentCount: 10 };
      return { currentCount: total % 10 };
    }
  }
  handleEdit = (record) => {
    // 初始化表单的值
    const { form } = this.formRef.props;
    form.setFieldsValue(record);
    // 显示编辑面板
    this.setState({ visible: true, fieldsValue: record });
  };
  handleDelete = (record) => {
    // 业务逻辑给父组件
    if (this.props.deleteData) {
      this.props.deleteData(record);
    }
  };

  handleSave = (row) => {
    const newData = [...this.state.dataSource];
    const index = newData.findIndex((item) => row.key === item.key);
    const item = newData[index];
    newData.splice(index, 1, {
      ...item,
      ...row
    });

    // 业务逻辑给父组件
    if (this.props.changeData) {
      this.props.changeData(newData[0]);
    }
  };

  render() {
    const { current } = this.state;
    const { totalCount } = this.props;
    const { currentCount } = this.handleTargetCount();
    const components = {
      body: {
        row: EditableFormRow,
        cell: EditableCell
      }
    };
    const columns = this.columns.map((col) => {
      if (!col.editable) {
        return col;
      }
      return {
        ...col,
        onCell: (record) => ({
          record,
          editable: col.editable,
          dataIndex: col.dataIndex,
          title: col.title,
          handleSave: this.handleSave
        })
      };
    });
    // 分页器
    const pagination = {
      total: this.props.totalCount,
      pageSize: this.props.pageSize,
      onChange: (currentPage) => {
        this.setState({ current: currentPage });
        if (this.props.getPageData) {
          this.props.getPageData(currentPage);
        }
      }
    };
    return (
      <div>
        <article
          style={{ marginTop: '30', marginBottom: '30px', padding: '20px 30px', background: 'rgb(120, 241, 247)' }}
        >
          <Icon type="exclamation-circle-o" style={{ marginRight: '20px' }} />
          <span>查询总计</span>
          {totalCount}
          <span>条，</span>
          <span>当前第</span>
          {current}
          <span>页，</span>
          <span>本页</span>
          {currentCount}
          <span>条</span>
        </article>
        <Table
          components={components}
          rowClassName={() => 'editable-row'}
          bordered
          dataSource={this.props.studentData}
          rowKey="id"
          columns={columns}
          pagination={pagination}
        />
        <CollectionCreateForm
          wrappedComponentRef={this.saveFormRef}
          visible={this.state.visible}
          onCancel={this.handleCancel}
          onCreate={this.handleCreate}
        />
      </div>
    );
  }
}
