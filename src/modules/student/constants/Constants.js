/** actionsc常量
 * @author liuqiubin
 * @time 2018/8/18
 * @version
 * @class
 */

// action types
export const INIT_DATA = 'INIT_DATA';
export const QUERY_DATA = 'QUERY_DATA';
export const CLEAR_DATA = 'CLEAR_DATA';
export const ADD_DATA = 'ADD_DATA';
export const GET_PAGE_DATA = 'GET_PAGE_DATA';
export const CHANGE_DATA = 'CHANGE_DATA';
export const DELETE_DATA = 'DELETE_DATA';
