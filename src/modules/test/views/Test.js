/**
 * @author zenglongfei
 * @time 2018/8/8
 * @version v3.0.0
 * @class
 */
import React, { PureComponent } from 'react';
import { connect } from 'dva';
import { Button } from 'antd';
import styles from './Test.less';

@connect(( { test } ) => ({
  data: test.data
}))
class Test extends PureComponent {
  onChange = () => {
    this.props.dispatch({ type: 'test/changeData' })
  }

  render() {
    const { data } = this.props;
    const { name = '', address = '' } = data;
    return (
      <div className={styles['huazi-test_span']}>
        <div>name: {name}</div>
        <div>address: {address}</div>
        <div>
          <Button type="primary" onClick={this.onChange}> 刷新</Button>
        </div>
      </div>
    )
  }
}
export default Test;
