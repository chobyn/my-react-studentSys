/* *
 * @author liuqiubin
 * @time 2018/8/13
 * @version
 * @class
 */

import { getData } from '../services/test'

export default {
  namespace: 'test',

  state: {
    data: {
      name: '华资',
      address: '广州'
    }
  },

  effects: {
    *changeData(_, { put, call }) {
      const data = yield call(getData);
      yield put ({
        type: 'changeDataEnd',
        payload: data
      })
    }
  },

  reducers: {
    changeDataEnd(state, { payload }) {
      return {
        ...state,
        data: { ...payload }
      }
    }
  }
};
