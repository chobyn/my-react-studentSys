/**
 * @author zenglongfei
 * @time 2018/8/8
 * @version v3.0.0
 * @class
 */
import { io } from 'SinoGear';
import { config } from '../../../common/config';

/**
 * 请求信息
 * @param val
 */
export const getData = () => {
  return io.post(`${config.contextPath}/api/test`);
}