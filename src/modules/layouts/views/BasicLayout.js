import React, { Component } from 'react';
import { Route, Redirect, Switch } from 'dva/router';
import { router as routerUtil } from '../../../utils/func';
/*基础布局架构*/
class BasicLayout extends Component {
  render() {
    const props = this.props;
    return (
      <Switch>
        {routerUtil
          .getRoutes(props.match.path, props.routerData)
          .map((item) => <Route key={item.key} path={item.path} component={item.component} exact={item.exact} />)}
        <Redirect exact from="/" to="/home" />
      </Switch>
    );
  }
}

export default BasicLayout;
