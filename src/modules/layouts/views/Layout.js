import React, { Component } from 'react';
import { connect } from 'dva';
import { Route, Switch, Redirect } from 'dva/router';
import { getRouterData } from '../../../common/router';

@connect((state) => ({
  layout: state.global.layout
}))
class Layout extends Component {
  render() {
    const { app, layout } = this.props;
    const routerData = getRouterData(app);
    const BasicLayout = routerData[`${layout['/']}`].component;
    return (
      <div>
        <Switch>
          <Route path="/" component={BasicLayout} />
          <Redirect to="/" />
        </Switch>
      </div>
    );
  }
}

export default Layout;
