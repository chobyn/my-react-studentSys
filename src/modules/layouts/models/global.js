import { store } from '../../../utils/func';
import { config } from '../../../common/config';

const sysConfigKey = 'sys_config';

export default {
  namespace: 'global',

  state: {
    layout: config.layout
  },

  effects: {
    *setLayout({ payload }, { call, put }) {
      yield put({
        type: 'setLayoutEnd',
        payload
      });
      const sysConfig = {};
      sysConfig.layout = payload;
      store.setItem(sysConfigKey, sysConfig, true);
    },
    *getLayout(_, { select, put }) {
      const global = yield select((state) => state.global);
      const sysConfig = store.getItem(sysConfigKey, true);
      const payload = (sysConfig && sysConfig.layout) || global.layout || {};
      if (payload) {
        yield put({
          type: 'setLayoutEnd',
          payload
        });
      }
    }
  },

  reducers: {
    setLayoutEnd(state, { payload }) {
      return {
        ...state,
        layout: payload
      };
    }
  }
};
