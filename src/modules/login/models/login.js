/**
 * @author zenglongfei
 * @time 2018/7/16
 * @version v3.0.0
 * @class
 */

import { addString, login } from '../services/login';

export default {
  namespace: 'login',

  state: {
    user: {}
  },

  effects: {
    *changeName({ payload }, { call, put }) {
      const val = yield call(login, payload);
      yield put({
        type: 'changeNameEnd',
        payload: val
      });
    }
  },

  reducers: {
    changeNameEnd(state, { payload }) {
      return {
        ...state,
        user: payload
      }
    }
  }
};
