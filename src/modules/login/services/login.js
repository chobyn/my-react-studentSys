/**
 * @author zenglongfei
 * @time 2018/7/16
 * @version v3.0.0
 * @class
 */

import { io } from 'SinoGear';
import { config } from '../../../common/config';

/**
 * 返回字符串
 * @param val
 * @returns {string} 拼接后字符串
 */
export const addString = (val) => {
  return `${val} long`
}

/**
 * 获取用户信息
 * @returns {Promise.<*|Promise>}
 */
export async function login() {
  return io.post(`${config.contextPath}/api/login`);
}
