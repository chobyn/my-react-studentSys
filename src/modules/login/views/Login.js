/**
 * @author zenglongfei
 * @time 2018/7/16
 * @version v3.0.0
 * @class
 */
import React, { PureComponent } from 'react';
import { connect } from 'dva';
import { Button } from 'antd';

import styles from './Login.less';

@connect(({ login }) => ({
  user: login.user
}))
class Login extends PureComponent {
  onChange = () => {
    this.props.dispatch({ type: 'login/changeName', payload: 'changeName' })
  };

  render() {
    const { user = '' } = this.props;
    return (
      <div className={styles['sg-login_header']}>
        <h1 className={styles.header_text}>feifei</h1>
        <h2>{user.name}</h2>
        <Button onClick={this.onChange}>登录</Button>
      </div>
    )
  }
}
export default Login;
