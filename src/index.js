import '@babel/polyfill';
import dva from 'dva';
import createHistory from 'history/createHashHistory';
import 'moment/locale/zh-cn';
import { SGLoading } from 'SinoGear';
import { Message } from './utils/Message';
import { tips, store, theme } from './utils/func';

import './index.less';

// 1. Initialize
const app = dva({
  history: createHistory(),
  onError(error) {
    const tipsError = tips.getErrorMsg(error);
    if (tipsError !== 'sg_error_401') {
      Message.msg({ type: 'error', content: tipsError });
    }
  }
});

// 2. Plugins
app.use(SGLoading.createLoading({ effects: true }));

// 3. Register global model
app.model(require('./modules/layouts/models/global').default);

// 4. Router
app.router(require('./router').default);

// 5. Start
app.start('#root');

const themeName = store.getItem('theme', true);
if (theme) {
  theme.changeTheme(themeName);
}
