const config = {
  name: 'SinoGear（赛姬）',
  username: '村民', // 当登录成功，sessionStorage中user_info不存在，或user_info.username不存在时默认显示名字
  messageDuration: 2.5,
  notificationDuration: 5,
  layout: {
    '/': '/BasicLayout'
  },
  jsonServerPath: 'http://localhost:3000',
  // contextPath不要修改，API的地址调用统一由代理进行。这里的地址默认dev模式下为空字符串''，仅在build前要修改为实际生产服务器地址
  contextPath: process.env.NO_PROXY !== 'true' ? 'http://localhost:3007' : process.env.contextPath || '' // IP配置 TODO contextPath名需重构
};

export { config };
