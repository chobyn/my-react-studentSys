import { createElement } from 'react';
import dynamic from 'dva/dynamic';
import Layout from '../modules/layouts/views/Layout';
import BasicLayout from '../modules/layouts/views/BasicLayout';
import HomePage from '../modules/home/views/HomePage';
import Login from '../modules/login/views/Login';
import Test from '../modules/test/views/Test';
import Test2 from '../modules/test2/views/Test2';
import Student from '../modules/student/views/index';
import { common as commonUtil } from '../utils/func';

let routerDataCache;

const modelNotExisted = (app, model) =>
  // eslint-disable-next-line
  !app._models.some(({ namespace }) => {
    return namespace === model.substring(model.lastIndexOf('/') + 1);
  });

// wrapper of dynamic
const dynamicWrapper = (app, models, component) => {
  const isQuoteComp = commonUtil.isFunc(component);
  // () => import('module') or () => require('module')
  // transformed by babel-plugin-dynamic-import-node-sync
  if (component.toString().indexOf('.then(') < 0 && !isQuoteComp) {
    models.forEach((model) => {
      if (modelNotExisted(app, model)) {
        // eslint-disable-next-line
        app.model(require(`../modules/${model}.js`).default);
      }
    });
    return (props) => {
      if (!routerDataCache) {
        const routerData = getRouterData(app);
        Object.keys(routerData).forEach((routerDataKey) => {
          if (routerDataKey.startsWith('/unused')) {
            delete routerData[routerDataKey];
          }
        });
        routerDataCache = routerData;
      }
      return createElement(component().default, {
        ...props,
        routerData: routerDataCache
      });
    };
  }
  // () => Component
  return dynamic({
    app,
    models: () => models.filter((model) => modelNotExisted(app, model)).map((m) => import(`../modules/${m}.js`)),
    // add routerData prop
    component: () => {
      if (!routerDataCache) {
        const routerData = getRouterData(app);
        Object.keys(routerData).forEach((routerDataKey) => {
          if (routerDataKey.startsWith('/unused')) {
            delete routerData[routerDataKey];
          }
        });
        routerDataCache = routerData;
      }
      let p;
      if (isQuoteComp) {
        p = component().default || component();
      } else {
        const comp = (async () => {
          const tempComp = await component();
          return tempComp;
        })();
        p = comp.default || comp;
      }
      return (props) =>
        createElement(p, {
          ...props,
          routerData: routerDataCache
        });
    }
  });
};

export const getRouterData = (app) => {
  const routerData = {
    '/': {
      component: dynamicWrapper(app, [], () => Layout)
    },
    '/BasicLayout': {
      component: dynamicWrapper(app, [], () => BasicLayout)
    },
    '/home': {
      component: dynamicWrapper(app, [], () => HomePage)
    },
    '/test': {
      component: dynamicWrapper(app, ['test/models/test', 'login/models/login'], () => Test)
    },
    '/test2': {
      component: dynamicWrapper(app, ['test2/models/test2'], () => Test2)
    },
    '/login': {
      component: dynamicWrapper(app, ['login/models/login'], () => Login)
    },
    '/student': {
      component: dynamicWrapper(app, [], () => Student)
    }
  };
  // Get name from ./menu.js or just set it in the router data.
  const routerDataWithName = {};
  Object.keys(routerData).forEach((item) => {
    routerDataWithName[item] = {
      ...routerData[item],
      name: routerData[item].name
    };
  });
  return routerDataWithName;
};
